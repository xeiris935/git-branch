<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use DB;

class PostsController extends Controller
{
    public function index(){
        // $posts = Post::all();
        // return Post::where('title', 'Post Two')->get();
        $posts = Post::orderBy('title', 'asc')->take(2)->get();
        // $posts = DB::select('SELECT * FROM posts');

        //pagination
        $posts = Post::orderBy('title', 'asc')->paginate(2);

        return view('posts.index')->with('posts', $posts);
    }

    public function show($id){
    	$post = Post::find($id);
    	return view('posts.show')->with('post', $post);
    }

    public function create(){
    	return view('/posts/create');
    }

    public function store(Request $request){

    	$this->validate($request, [
    		'title' => 'required',
    		'body' => 'required'
    	]);

    	$post = new Post;
    	$post->title = $request->title;
    	$post->body = $request->body;
        $post->user_id = auth()->user()->id;
    	$post->save();

    	return redirect('/posts');
    }

    public function edit($id){
    	$post = Post::find($id);
    	return view('posts.edit')->with('post', $post);
    }

    public function update(Request $request, $id){

		$this->validate($request, [
    		'title' => 'required',
    		'body' => 'required'
    	]);

    	$post = Post::find($id);
    	$post->title = $request->title;
    	$post->body = $request->body;
    	$post->save();
    	return redirect('/posts');
    }

    public function destroy($id){
    	$post = Post::find($id);
    	$post->delete();
    	return redirect('/posts');
    }
}