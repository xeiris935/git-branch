<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/about', 'PagesController@about');
Route::get('/services', 'PagesController@services');

// Route::get('/hello', function() {
// 	return view('hello');
// });

Route::get('/hello', 'PagesController@hello');
Route::resource('/posts', 'PostsController');

Route::get('/profile', function() {
	return view('miniprofile');
});

Route::get('/hobbies', function() {
	return view('hobbies');
});

Route::get('/dreams', function() {
	return view('dreams');
});

// directives @extends
// blade.php templating engine
	// will not work with {{}}
// objectives and match objectives at end of day
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
