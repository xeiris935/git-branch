@extends('layouts.app')

@section('content')
    <h1>Posts</h1>
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div class="well">
                <h3>
                    <a href="/posts/{{$post->id}}">
                        {{$post->title}}
                    </a>
                </h3>
                <small>
                    Written on {{$post->created_at}}
                </small>
                <div class="d-flex mx-3">
                    <a href="/posts/{{$post->id}}/edit" class="btn btn-primary">Edit Post</a>
                    <form action="/posts/{{$post->id}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button type="submit" class="btn btn-danger">Delete Item</button>
                    </form>
                </div>
            </div>
        @endforeach
        {{$posts->links()}}
    @else
      <p>No posts found</p>
    @endif
@endsection