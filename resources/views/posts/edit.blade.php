@extends('layouts.app')

@section('content')
	<h1>Edit Post</h1>
	<form action="/posts/{{$post->id}}" method="POST">
		@csrf
		@method('PUT')
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control" value="{{$post->title}}">
		</div>
		<div class="form-group">
			<label for="body">Body</label>
			<textarea name="body" class="form-control">{{$post->body}}</textarea>
		</div>
		<button type="submit">Edit Post</button>
	</form>
@endsection