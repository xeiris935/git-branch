@extends('layouts.app')

@section('content')
	<h1>Create Post</h1>

	<form action="/posts" method="POST">
		@csrf
		<div class="form-group">
			<label for="title">Title</label>
			<input type="text" name="title" class="form-control" placeholder="Title">
		</div>
		<div class="form-group">
			<label for="body">Body</label>
			<textarea name="body" class="form-control"></textarea>
		</div>
		<button type="submit" class="btn btn-primary">Add Post</button>
	</form>
	
@endsection