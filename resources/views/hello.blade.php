<!DOCTYPE html>
<html>
<head>
	<title>Another page</title>
</head>
<body>
	<h1>This is the third page!!</h1>
	<p>{{$front_end}}</p>
	@if(count($topics) > 0)
		@foreach($topics as $topic)
			<li>{{$topic}}</li>
		@endforeach
	@endif
</body>
</html>